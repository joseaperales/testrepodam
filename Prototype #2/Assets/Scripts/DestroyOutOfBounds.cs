﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOutOfBounds : MonoBehaviour
{
    public float topBound = 30;
    public float lowerBounb = -15;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //condicion: si este objeto sobrepasa topBound, entonces se destruye.
        if (transform.position.z >= topBound)
            Destroy(gameObject);

        if (transform.position.z <= lowerBounb)
            Destroy(gameObject);
    }
}
