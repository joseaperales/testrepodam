﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float xRange = 10;
    public float zRange = 0.5f;
    public float speed = 10.0f;
    public GameObject projectilePrefab, projectilePrefab2;

    float count = 0;
    float horizontalInput;
    float verticalInput;
    Vector3 currentOffset;
    
    private void Start()
    {
        currentOffset = transform.position;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && count % 2 != 0)
        {
            Instantiate(projectilePrefab, transform.position, projectilePrefab.transform.rotation);
            count++;
        }
        else if (Input.GetKeyDown(KeyCode.Space) && count % 2 == 0)
        {
            Instantiate(projectilePrefab2, transform.position, projectilePrefab2.transform.rotation);
            count--;
        }

        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
        transform.Translate(Vector3.right * horizontalInput * Time.deltaTime * speed);
        transform.Translate(Vector3.forward * verticalInput * Time.deltaTime * speed);

        //entra en el if cuando me salgo por la izquierda
        if (transform.position.x < currentOffset.x - xRange)
            transform.position = new Vector3(currentOffset.x - xRange, transform.position.y, transform.position.z);
        //entra en el if cuando me salgo por la derecha
        if (transform.position.x > currentOffset.x + xRange)
            transform.position = new Vector3(currentOffset.x + xRange, transform.position.y, transform.position.z);
        //entro en el if cuando me salgo por delante
        if (transform.position.z > currentOffset.z + zRange)
            transform.position = new Vector3(transform.position.x, transform.position.y, currentOffset.z + zRange);
        //entro en el if cuando me salgo por detras
        if (transform.position.z < currentOffset.z - zRange)
            transform.position = new Vector3(transform.position.x, transform.position.y, currentOffset.z - zRange);

    }
}
