﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerTank : MonoBehaviour
{
    public float movementSpeed = 5;
    public float turnSpeed = 60;
    float forwardInput;
    float turnInput;

    // Update is called once per frame
    void Update()
    {
        turnInput = Input.GetAxis("Horizontal"); //a = -1, d = 1
        forwardInput = Input.GetAxis("Vertical");

        transform.Translate(Vector3.up * Time.deltaTime * forwardInput * movementSpeed);
        transform.Rotate(Vector3.forward * Time.deltaTime * -turnSpeed * turnInput);
    }

}
