﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController2D : MonoBehaviour
{
    public float verticalSpeed = 5;
    public float horizontalSpeed = 10;
    float horizontalInput;
    float verticalInput;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        transform.Translate(
            Vector3.right * horizontalSpeed * Time.deltaTime * horizontalInput);
        transform.Translate(
            Vector3.up * verticalSpeed * Time.deltaTime * verticalInput);
    }

}
